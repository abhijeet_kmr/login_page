// This is sign Up.js file.
// Created variables for DOM Manipulation.
const signUpForm = document.getElementById("signUp-form");
const emailErrorMsg = document.getElementById("email-error-msg");
const passwordErrorMsg = document.getElementById("password-error-msg");
const submitForm = document.getElementById("signUp-form-submit");

function validateEmail(email) {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return true;
  }
  return false;
}

function validatePassword(password) {
  if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(.{10,15})$/.test(password)) {
    return true;
  }
  return false;
}

function validUser(email, password) {
  let isValid;
  data.find((info) => {
    if (info.email === email && info.password === password) {
      isValid = true;
    } else if (info.email === email && info.password != password) {
      isValid = false;
    }
  });
  return isValid;
}

// Initialised the click event for Sign Up button
submitForm.addEventListener("click", (e) => {
  e.preventDefault();
  const email = signUpForm.email.value;
  const password = signUpForm.password.value;

  if (!validateEmail(email)) {
    return (emailErrorMsg.style.display = "block");
  }
  if (validateEmail(email)) {
    emailErrorMsg.style.display = "none";
  }

  if (!validatePassword(password)) {
    return (passwordErrorMsg.style.display = "block");
  }
  if (validatePassword(password)) {
    passwordErrorMsg.style.display = "none";
  }

  fetch("http://127.0.0.1:3000/signup", {
    method: "POST",
    body: JSON.stringify({ email, password }),
    headers: { "Content-Type": "application/json" },
  })
    .then((res) => res.json())
    .then(() => {
      window.location = "success.html";
    });
});
