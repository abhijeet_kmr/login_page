const express = require("express");
const app = express();
const fs = require("fs");
const cors = require("cors");
const path = require("path");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

// User data validaton function
const validUser = (data, email, password) => {
  let isValid = false;
  data.find((info) => {
    if (info.email === email && info.password === password) {
      isValid = true;
    }
  });
  return isValid;
};

// Get call for the server endpoint.
app.get("/", (req, res) => {
  fs.readFile(path.join(__dirname, "./data.json"), (err, data) => {
    if (err) {
      res.send(err);
    } else {
      res.send(JSON.parse(data));
    }
  });
});

// Transmitting the user login data.
app.post("/login", jsonParser, (req, res) => {
  let email = req.body.email;
  let password = req.body.password;
  fs.readFile(
    path.join(__dirname, "./data.json"),
    { encoding: "utf-8" },
    (err, data) => {
      if (err) {
        console.log(err);
      } else {
        const newData = JSON.parse(data);
        if (validUser(newData, email, password)) {
          res.send({ success: true });
        } else {
          res.send({ success: false });
        }
      }
    }
  );
});

// Transmitting the user sign up data and updating the old data.
app.post("/signup", (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  fs.readFile(
    path.join(__dirname, "./data.json"),
    { encoding: "utf-8" },
    (err, data) => {
      if (err) {
        console.log(err);
      } else {
        const receivedData = JSON.parse(data);
        const updater = (receivedData) => {
          const obj = { email: email, password: password };
          receivedData.push(obj);
          return receivedData;
        };
        const newData = updater(receivedData);
        fs.writeFile(
          path.join(__dirname, "./data.json"),
          JSON.stringify(newData),
          (err) => {
            if (err) {
              console.log(err);
            }
          }
        );
      }
    }
  );
  res.send({ success: true });
});

// Listening the server on port.
app.listen(3000, () => console.log("Listening on port 3000"));
